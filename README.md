# Docker Debian Stretch - RVM - Rails - CodeTriage

This repository is used for building a custom Docker image for [CodeTriage](https://github.com/codetriage/codetriage).

## Name of This Docker Image
[registry.gitlab.com/rubyonracetracks/docker-debian-stretch-rvm-rails-codetriage](https://gitlab.com/rubyonracetracks/docker-debian-stretch-rvm-rails-codetriage/container_registry)

## Upstream Docker Image
[registry.gitlab.com/rubyonracetracks/docker-debian-stretch-min-rvm](https://gitlab.com/rubyonracetracks/docker-debian-stretch-min-rvm/container_registry)

## What's Added
* The latest version of Ruby
* The latest versions of the rails, pg, nokogiri, and ffi gems
* The versions of the above gems and Ruby used in the CodeTriage app
* Bundler
* The mailcatcher gem

## Things NOT Included
This Docker image does not include all versions of Ruby, Rails, pg, nokogiri, and ffi.  Instead, I have custom Docker images for every Rails app I'm working on.

## What's the Point?
This Docker image is used for developing the CodeTriage app.

## More Information
General information common to all Docker Debian build repositories is in the [FAQ](https://gitlab.com/rubyonracetracks/docker-debian-common/blob/master/FAQ.md).
